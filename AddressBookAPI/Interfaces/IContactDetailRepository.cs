﻿using AddressBookAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBookAPI.Interfaces
{
    public interface IContactDetailRepository
    {
        Task<ActionResult<IEnumerable<ContactDetail>>> GetContactDetailsAsync();
        Task<ActionResult<ContactDetail>> GetContactDetailAsync(int id);
        Task<ActionResult<ContactDetail>> PostContactDetailAsync(ContactDetail contactDetail);
        Task<IActionResult> DeleteContactDetailAsync(int id);
        Task<IActionResult> PutContactDetailAsync(int id, ContactDetail contactDetail);


    }
}
