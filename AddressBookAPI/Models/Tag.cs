﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBookAPI.Models
{
    public class Tag
    {
        [Key]
        public int TagId { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string TagName { get; set; }
        public ICollection<ContactTag> Contacts { get; set; }

    }
}
