﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBookAPI.Models
{
    public class ContactDetail
    {
        [Key]
        public int ContactId { get; set; }

        [Column(TypeName ="nvarchar(100)")]
        public string FullName { get; set; }
        
        [Column(TypeName = "nvarchar(100)")]
        public string Email { get; set; }

        [Column(TypeName = "nvarchar(15)")]
        public string Phone { get; set; }

        [Column(TypeName = "nvarchar(100)")] 
        public string Title { get; set; }

        [Column(TypeName = "nvarchar(100)")] 
        public string Company { get; set; }
        
        [Column(TypeName = "nvarchar(100)")] 
        public string LinkedIn { get; set; }

        [Column(TypeName = "nvarchar(100)")] 
        public string Skype { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public ICollection<ContactTag> Tags { get; set; }
    }
}
