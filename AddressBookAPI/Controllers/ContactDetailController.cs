﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AddressBookAPI.Models;
using Microsoft.AspNetCore.Cors;
using AddressBookAPI.DataAccess;
using AddressBookAPI.Interfaces;

namespace AddressBookAPI.Controllers
{
    [EnableCors("Policy1")]
    [Route("api/[controller]")]
    [ApiController]
    public class ContactDetailController : ControllerBase
    {
        private readonly IContactDetailRepository _contactDetails;

        public ContactDetailController(IContactDetailRepository contactDtls)
        {
            _contactDetails = contactDtls;
        }

        // GET: api/ContactDetail
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContactDetail>>> GetContactDetails()
        {
            return await _contactDetails.GetContactDetailsAsync();
        }

        // GET: api/ContactDetail/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ContactDetail>> GetContactDetail(int id)
        {
            return await _contactDetails.GetContactDetailAsync(id);
        }

        // PUT: api/ContactDetail/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutContactDetail(int id, ContactDetail contactDetail)
        {
            return await _contactDetails.PutContactDetailAsync(id, contactDetail);
        }

        // POST: api/ContactDetail
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<ContactDetail>> PostContactDetail(ContactDetail contactDetail)
        {
            return await _contactDetails.PostContactDetailAsync(contactDetail);
        }

        // DELETE: api/ContactDetail/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContactDetail(int id)
        {
            return await _contactDetails.DeleteContactDetailAsync(id);
        }
    }
}
