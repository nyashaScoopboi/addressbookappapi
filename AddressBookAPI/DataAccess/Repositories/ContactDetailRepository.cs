﻿using AddressBookAPI.Interfaces;
using AddressBookAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBookAPI.DataAccess.Repositories
{
    public class ContactDetailRepository :  ControllerBase, IContactDetailRepository
    {
        private readonly AppDbContext _context;
        public ContactDetailRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> DeleteContactDetailAsync(int id)
        {
            var contactDetail = await _context.ContactDetails.FindAsync(id);
            if (contactDetail == null)
            {
                return NotFound();
            }

            _context.ContactDetails.Remove(contactDetail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        public async Task<ActionResult<ContactDetail>> GetContactDetailAsync(int id)
        {
            var contactDetail = await _context.ContactDetails.FindAsync(id);

            if (contactDetail == null)
            {
                return NotFound();
            }

            return contactDetail;
        }

        public async Task<ActionResult<IEnumerable<ContactDetail>>> GetContactDetailsAsync()
        {
            return await _context.ContactDetails.ToListAsync();
        }

        public async Task<ActionResult<ContactDetail>> PostContactDetailAsync(ContactDetail contactDetail)
        {
            _context.ContactDetails.Add(contactDetail);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetContactDetail", new { id = contactDetail.ContactId }, contactDetail);
        }

        public async Task<IActionResult> PutContactDetailAsync(int id, ContactDetail contactDetail)
        {
            if (id != contactDetail.ContactId)
            {
                return BadRequest();
            }

            _context.Entry(contactDetail).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactDetailExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool ContactDetailExists(int id)
        {
            return _context.ContactDetails.Any(e => e.ContactId == id);
        }
    }
}
