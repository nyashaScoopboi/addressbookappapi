﻿using AddressBookAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AddressBookAPI.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<ContactDetail> ContactDetails { get; set; }
        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ContactTag>()
                .HasKey(ct => new { ct.ContactId, ct.TagId });
            builder.Entity<ContactTag>()
                .HasOne(ct => ct.ContactDetail)
                .WithMany(ct => ct.Tags)
                .HasForeignKey(ct => ct.ContactId);
            builder.Entity<ContactTag>()
                .HasOne(ct => ct.Tag)
                .WithMany(ct => ct.Contacts)
                .HasForeignKey(ct => ct.TagId);
        }

    }
}
